# Fibonacci Huge Problem

Exercise included in the Algorithmic Warmup lecture of Algorithm 
Toolbox course on Cousera.  

This problem asks to compute $`F_{n}`$ modulo $`m`$, where $`n`$ can be as 
huge as $10^{14}$. To accomodate such a large number of $n$, any 
algorithm that iterates to $n$ will be too slow for sure. 

The key for solving this problem with an algorithm that does not need 
to loops $n$ times is to note that the sequences of Fibonacci numbers 
$F_{n}$ modulo $m$ are periodic for any integer $m\geq2$. Therefore, an 
efficient algorithm only needs to iterate a few times to find out the 
period of the sequence, and can then use the periodicity to quickly 
compute $F_{n}$ modulo $m$ for any large $n$.
