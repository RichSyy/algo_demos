# Uses python3
import sys

def get_fibonacci_huge_naive(n, m):
    if n <= 1:
        return n

    previous = 0
    current  = 1

    for _ in range(n - 1):
        previous, current = current, previous + current

    return current % m


def get_fib_huge_efficient(n, m):
    fib = [0, 1]
    fib_mod = [0, 1]
    period = 2
    i = 2

    while True: # will break at some point
        new_fib = fib[i - 2] + fib[i - 1]
        fib.append(new_fib)
        new_fib_mod = new_fib % m # modulo of the fib number
        prev_fib_mod = fib_mod[-1]
        fib_mod.append(new_fib_mod)
        period += 1 # increase period count
        i += 1
        if new_fib_mod == 1 and not prev_fib_mod:
            break

    period -= 2 # added additional 2 in the loop
    equavalent_n = n % period # by periodic
    return fib_mod[equavalent_n]


if __name__ == '__main__':
    input = sys.stdin.read()
    n, m = map(int, input.split())
    print(get_fib_huge_efficient(n, m))
