import pytest
from fibonacci import *


@pytest.fixture
def fib_numbers():
    return {
        3: 2,
        5: 5,
        7: 13, 
        12: 144,
        16: 987,
        18: 2584,
        20: 6765,
        36: 14930352,
        40: 102334155,
        50: 12586269025,
    }


def test_calc_fib(fib_numbers):
    for n, result in fib_numbers.items():
        assert result == calc_fib(n)