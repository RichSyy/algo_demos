# python3
import sys


def compute_min_refills(distance, tank, stops):
    # write your code here
    n = len(stops)
    num_refills = 0
    last_idx = 0 # index of the last stop
    crt_idx = 0 # index of the current stop
    all_stops = [0] + stops + [distance]
    while crt_idx < n + 1:
        
        while all_stops[crt_idx + 1] - all_stops[last_idx] < tank:
            crt_idx += 1
            if crt_idx == n + 1:
                return num_refills # already reached destination

        if crt_idx == last_idx:
            return -1 # distance is not reachable
        else: 
            last_idx = crt_idx # confirm we refill at crt_idx
            num_refills += 1

    return num_refills

if __name__ == '__main__':
    d, m, _, *stops = map(int, sys.stdin.read().split())
    print(compute_min_refills(d, m, stops))
