import pytest
from car_fueling import compute_min_refills

@pytest.fixture
def cases():
    case1 = {
        'dist': 950,
        'tank': 400,
        'stops': [200, 375, 550, 750],
        'res': 2
    }
    case2 = {
        'dist': 10,
        'tank': 3,
        'stops': [1, 2, 5, 9],
        'res': -1
    }
    case3 = {
        'dist': 200,
        'tank': 250,
        'stops': [100, 150],
        'res': 0
    }
    return [case1, case2, case3]

def test_compute_min_refills(cases):
    for case in cases:
        dist, tank, stops = case['dist'], case['tank'], case['stops']
        assert compute_min_refills(dist, tank, stops) == case['res']

