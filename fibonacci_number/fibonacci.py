# Uses python3
def calc_fib(n):
    fib = [0, 1]
    if n <= 1:
        return fib[n]
    for i in range(2, n + 1):
        fib.append(fib[i - 2] + fib[i - 1])
    return fib[n]


if __name__ == "__main__":
    n = int(input())
    print(calc_fib(n))
