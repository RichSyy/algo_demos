# Fibonacci Number Problem

Exercise included in the Algorithmic Warmup lecture in the Algorithm 
Toolbox course on Coursera. 

This is a classic problem for illustrating recursive algorithms and how 
time-consuming and inefficient they can be. The natural algorithm is to 
simply recursively call the function and go all the way to the base 
cases of $F_{0} = 0$ and $F_{1}=1$. However, the problem for this naive 
algorithm is that we are calculating the same numbers over and over 
again, and as $n$ gets large, the time it needs grows exponentially. 

Since we are computing the same numbers repeatedly, one simple 
improvement is to record the already calculated numbers in an array and 
look up when we need to compute a new one. This wey we have an 
efficient algorithm that's no longer recursive but iterative, and has 
polynomial time complexity.
