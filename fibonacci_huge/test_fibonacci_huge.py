import pytest
from fibonacci_huge import *


@pytest.fixture
def test_cases():
    return {
        (9, 2): 0,
        (10, 3): 1,
        (10, 4): 3,
        (13, 4): 1,
        (3, 3): 2,
        (12, 2): 0,
        (12, 4): 0,
    }


def test_fib_huge(test_cases):
    for test, result in test_cases.items():
        assert result == get_fib_huge_efficient(*test)