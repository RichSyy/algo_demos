# Car Fueling Problem

Exercise of the Greedy Algorithm lecture in the Algorithm Toolbox 
course on Coursera.

A car needs to get from A to B. Along the way there are numerous gas 
stations where the car may stop for fuel. Given the total distance from 
A to B, the capacity of a full tank of fuel, and the array indicating 
the distance of the gas stations from point A, devise an algorithm to 
calculate the minimum number of refuels.

* Greedy Choice:  
Refuel at the farthest reachable gas station only.

* Subproblem after first move:  
Starting at point A, suppose we refuel at gas station G that is the 
farthest reachable, according to the greedy choice. The problem reduces 
to now getting from point G to B, which is very much the same as the 
original problem but easier to solve.
