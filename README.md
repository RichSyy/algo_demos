# algo_demos

This is for storing all kinds of algorithm demos I've made in my 
learning process. The first ones would be the examples and exercise 
from the 
[_Algorithm Toolbox_](https://www.coursera.org/learn/algorithmic-toolbox) 
course on Coursera.
